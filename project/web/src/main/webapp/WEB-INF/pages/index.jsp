<!DOCTYPE html>
<html>
<head>
	 <link rel="stylesheet" type="text/css" href="estilo.css">
</head>
<body id="active3">
<div id="header">
	<h1>Maven + Spring MVC Web Project Example</h1>
	<h3>Message : ${message}</h3>
	<h3>Counter : ${counter}</h3>
</div>
<div id="container3">
	<div id="container2">
		<div id="container1">
			<div id="col1">
				<!-- Column one start -->
				<h2>Equal height columns</h2>
				<p>It does not matter how much content is in each column, the 
background colours will always stretch down to the height of the tallest
 column.</p>
				<h2>3 Column Dimensions</h2>
				<p>Each column is 33.33% percent wide with 2 percent padding on each side.</p>
				<h2>No CSS hacks</h2>
				<p>The CSS used for this 3 column layout is 100% valid and hack 
free. To overcome Internet Explorer's broken box model, no horizontal 
padding or margins are used. Instead, this design uses percentage widths
 and clever relative positioning.</p>
				<h2>No JavaScript</h2>
				<p>JavaScript is not required. Some website layouts rely on 
JavaScript hacks to resize divs and force elements into place but you 
won't see any of that nonsense here.</p>
				<!-- Column one end -->
			</div>
			<div id="col2">
				CENTER
				<!-- Column two end -->
			</div>
			<div id="col3">
				Right
				<!-- Column three end -->
			</div>
		</div>
	</div>
</div>
<div id="footer">
	<p> <center><a href="http://leitmotiff.com">leitmotiff 2015 </a>
</div>



</body></html>
