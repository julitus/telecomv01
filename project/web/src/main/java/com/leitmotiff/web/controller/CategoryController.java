package com.leitmotiff.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;

import com.leitmotiff.business.category.ICategoryManager;
import com.leitmotiff.business.category.dto.CategoryDTO;

@Controller
public class CategoryController {

	@Autowired private ICategoryManager categoryManager;

	// TODO Revisar RequestBody de spring
	// public @RequestBody List<CategoryDTO> listCategories() {
	@RequestMapping(value="categories", method=RequestMethod.GET)
	public List<CategoryDTO> listCategories() {	
		return categoryManager.getCategories();
	}
	
}
