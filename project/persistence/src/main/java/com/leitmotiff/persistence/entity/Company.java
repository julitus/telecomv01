package com.leitmotiff.persistence.entity;

// Generated Mar 6, 2015 8:56:33 AM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Company generated by hbm2java
 */
@Entity
@Table(name = "company", catalog = "telecom")
public class Company implements java.io.Serializable {

	private Long companyId;
	private String companyName;
	private String companyDescription;
	private String companyShortname;
	private String companyUrl;
	private Set<Review> reviews = new HashSet<Review>(0);
	private Set<ProductHasCompany> productHasCompanies = new HashSet<ProductHasCompany>(
			0);
	private Set<Product> products = new HashSet<Product>(0);

	public Company() {
	}

	public Company(String companyName, String companyDescription,
			String companyShortname, String companyUrl) {
		this.companyName = companyName;
		this.companyDescription = companyDescription;
		this.companyShortname = companyShortname;
		this.companyUrl = companyUrl;
	}

	public Company(String companyName, String companyDescription,
			String companyShortname, String companyUrl, Set<Review> reviews,
			Set<ProductHasCompany> productHasCompanies, Set<Product> products) {
		this.companyName = companyName;
		this.companyDescription = companyDescription;
		this.companyShortname = companyShortname;
		this.companyUrl = companyUrl;
		this.reviews = reviews;
		this.productHasCompanies = productHasCompanies;
		this.products = products;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "company_id", unique = true, nullable = false)
	public Long getCompanyId() {
		return this.companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	@Column(name = "company_name", nullable = false)
	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@Column(name = "company_description", nullable = false, length = 65535)
	public String getCompanyDescription() {
		return this.companyDescription;
	}

	public void setCompanyDescription(String companyDescription) {
		this.companyDescription = companyDescription;
	}

	@Column(name = "company_shortname", nullable = false, length = 20)
	public String getCompanyShortname() {
		return this.companyShortname;
	}

	public void setCompanyShortname(String companyShortname) {
		this.companyShortname = companyShortname;
	}

	@Column(name = "company_url", nullable = false, length = 45)
	public String getCompanyUrl() {
		return this.companyUrl;
	}

	public void setCompanyUrl(String companyUrl) {
		this.companyUrl = companyUrl;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
	public Set<Review> getReviews() {
		return this.reviews;
	}

	public void setReviews(Set<Review> reviews) {
		this.reviews = reviews;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
	public Set<ProductHasCompany> getProductHasCompanies() {
		return this.productHasCompanies;
	}

	public void setProductHasCompanies(
			Set<ProductHasCompany> productHasCompanies) {
		this.productHasCompanies = productHasCompanies;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
	public Set<Product> getProducts() {
		return this.products;
	}

	public void setProducts(Set<Product> products) {
		this.products = products;
	}

}
