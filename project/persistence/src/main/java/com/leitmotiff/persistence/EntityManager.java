package com.leitmotiff.persistence;

public interface EntityManager {
	
	/**
	 * This method persist an Entity object
	 * 
	 * @param obj Entity to persist
	 * @return result
	 */
	<T> boolean persist(T obj);
	
	/**
	 * This method find an Entity in Database
	 * 
	 * @param cl
	 * @param id
	 * @return
	 */
	<T> T find(Class<T> cl, int id);
	
}
