package com.leitmotiff;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import com.leitmotiff.business.topProduct.TopProductManager;
import com.leitmotiff.business.topProduct.dto.TopProductDTO;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite();
    }

    /**
     * Rigourous Test :-)
     * @throws SQLException 
     */
	public void testApp() throws SQLException
    {
        assertTrue( true );
        
        String _usuario="root";
        String _pwd= "021-14109";
        String _bd="mysql";
        String _url = "jdbc:mysql://localhost/"+_bd;
        
        Connection conn = null;      
        try {
			conn =(Connection) DriverManager.getConnection(_url,_usuario, _pwd);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			System.out.println("Problema en conexxion BD");
		}
        Statement state=null;
		try {
			state = (Statement)conn.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			System.out.println("Problema en statement");
		}
        ResultSet resultado= null;
		try{
			//state = (Statement) conn.createStatement();
			resultado = state.executeQuery("SELECT * FROM Personas");
			}
			catch(SQLException e)
			{
			e.printStackTrace();
		}
        
        try {
			while(resultado.next()){
				System.out.println(resultado.getString("name"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
///////////////////////////////////////////////////
    }

}
