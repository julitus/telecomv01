package com.leitmotiff.business.topProduct;

import java.util.List;

import com.leitmotiff.business.category.dto.CategoryDTO;
import com.leitmotiff.business.topProduct.dto.TopProductDTO;
import com.leitmotiff.persistence.entity.TopProduct;

public interface ITopProductManager {
	
	public List<TopProductDTO> getProducts();
	public TopProductDTO mappingDTO(TopProduct p);
	public TopProduct mapping(TopProductDTO p);
	
}
