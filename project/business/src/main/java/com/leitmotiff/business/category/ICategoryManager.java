package com.leitmotiff.business.category;

import java.util.List;

import com.leitmotiff.business.category.dto.CategoryDTO;
import com.leitmotiff.persistence.entity.ProductCategory;

public interface ICategoryManager {

	public List<CategoryDTO> getCategories();
	public void add(CategoryDTO c);
	public ProductCategory mapping(CategoryDTO c);
	public CategoryDTO mappingDTO(ProductCategory c);
}
