package com.leitmotiff.business.topProduct;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.springframework.stereotype.Service;

import com.leitmotiff.business.product.ProductManager;
import com.leitmotiff.business.topProduct.dto.TopProductDTO;
import com.leitmotiff.persistence.entity.TopProduct;

@Service("topProduct")
public class TopProductManager implements ITopProductManager {
	
	private EntityManager manager; 
	
	public TopProductManager(){
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("telecom");
		manager = factory.createEntityManager();
	}
	
	public List<TopProductDTO> getProducts(){
		
		Query query = manager.createQuery("select t from TopProduct as t ");
		
		List<TopProduct> lista = query.getResultList();
		List<TopProductDTO> listaDTO = new ArrayList<TopProductDTO>();
		
		for (TopProduct t : lista){
			System.out.println(t.getTopProductId() + " - " + t.getTopProductPositive() +
					" - " + t.getTopProductNegative() + " - " + t.getProduct().getProductName() +
					" - " + t.getProduct().getProductCategory().getProductCategoryName());
			listaDTO.add(mappingDTO(t));
		}
		
		manager.close();
		return listaDTO;
		
	}
	
	public TopProductDTO mappingDTO(TopProduct topProduct){
		
		TopProductDTO topProductDto = new TopProductDTO();
		topProductDto.setId(topProduct.getTopProductId());
		topProductDto.setPositive(topProduct.getTopProductPositive());
		topProductDto.setNegative(topProduct.getTopProductNegative());
		topProductDto.setProduct(new ProductManager().mappingDTO(topProduct.getProduct()));
		return topProductDto;
		
	}
	
	public TopProduct mapping(TopProductDTO topProductDto){
		
		TopProduct topProduct = new TopProduct();
		topProduct.setTopProductId(topProductDto.getId());
		topProduct.setTopProductPositive(topProductDto.getPositive());
		topProduct.setTopProductNegative(topProductDto.getNegative());
		topProduct.setProduct(new ProductManager().mapping(topProductDto.getProduct()));
		return topProduct;
		
	}
	
}
