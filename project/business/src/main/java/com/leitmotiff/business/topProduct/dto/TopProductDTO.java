package com.leitmotiff.business.topProduct.dto;

import com.leitmotiff.business.product.dto.ProductDTO;

public class TopProductDTO {

	private Integer id;
	private Integer positive;
	private Integer negative;
	private ProductDTO product
	;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getPositive() {
		return positive;
	}
	public void setPositive(Integer positive) {
		this.positive = positive;
	}
	public Integer getNegative() {
		return negative;
	}
	public void setNegative(Integer negative) {
		this.negative = negative;
	}
	public ProductDTO getProduct() {
		return product;
	}
	public void setProduct(ProductDTO product) {
		this.product = product;
	}
	
}
