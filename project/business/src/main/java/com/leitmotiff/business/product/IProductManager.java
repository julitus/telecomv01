package com.leitmotiff.business.product;

import java.util.List;

import com.leitmotiff.business.category.dto.CategoryDTO;
import com.leitmotiff.business.product.dto.ProductDTO;
import com.leitmotiff.persistence.entity.Product;

public interface IProductManager {
	
	public List<ProductDTO> getProducts();
	public List<ProductDTO> getProducts(CategoryDTO c);
	public ProductDTO mappingDTO(Product p);
	public Product mapping(ProductDTO p);
	
}
