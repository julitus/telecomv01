package com.leitmotiff.business.category;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.springframework.stereotype.Service;

import com.leitmotiff.business.category.dto.CategoryDTO;
import com.leitmotiff.persistence.entity.ProductCategory;

@Service("category")
public class CategoryManager implements ICategoryManager {

	private EntityManager manager; 

	public CategoryManager(){
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("telecom");
		manager = factory.createEntityManager();
	}
	
	@Override
	public List<CategoryDTO> getCategories() {
		
		Query query = manager.createQuery("select t from ProductCategory as t ");
		
		List<ProductCategory> lista = query.getResultList();
		List<CategoryDTO> listaDTO = new ArrayList<CategoryDTO>();
		
		for (ProductCategory t : lista){
			System.out.println(t.getProductCategoryId() + " - " + t.getProductCategoryName());
			listaDTO.add(mappingDTO(t));
		}
		
		manager.close();
		return listaDTO;
	}
	
	public void add(CategoryDTO categoryDto){
		
		/*ProductCategory category = (ProductCategory) MappingFactory.defaultMapper().map(categoryDto, new ProductCategory());*/

	    manager.getTransaction().begin();    
	    manager.persist(mapping(categoryDto));
	    manager.getTransaction().commit();  

	    System.out.println("regitrado!");

	    manager.close();
	}
	
	public ProductCategory mapping(CategoryDTO category){
		ProductCategory productCategory = new ProductCategory();
		productCategory.setProductCategoryId(category.getId());
		productCategory.setProductCategoryName(category.getName());
		return productCategory;
	}
	
	public CategoryDTO mappingDTO(ProductCategory productCategory){
		CategoryDTO category = new CategoryDTO();
		category.setName(productCategory.getProductCategoryName());
		category.setId(productCategory.getProductCategoryId());
		return category;
	}
}
