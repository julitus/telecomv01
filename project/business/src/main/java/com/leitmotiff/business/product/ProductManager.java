package com.leitmotiff.business.product;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.springframework.stereotype.Service;

import com.leitmotiff.business.category.CategoryManager;
import com.leitmotiff.business.category.dto.CategoryDTO;
import com.leitmotiff.business.product.dto.ProductDTO;
import com.leitmotiff.persistence.entity.Product;

@Service("product")
public class ProductManager implements IProductManager{
	
	private EntityManager manager; 
	
	public ProductManager(){
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("telecom");
		manager = factory.createEntityManager();
	}
	
	public List<ProductDTO> getProducts(){
		
		Query query = manager.createQuery("select t from Product as t ");
		
		List<Product> lista = query.getResultList();
		List<ProductDTO> listaDTO = new ArrayList<ProductDTO>();
		
		for (Product t : lista){
			System.out.println(t.getProductId() + " - " + t.getProductName());
			listaDTO.add(mappingDTO(t));
		}
		
		manager.close();
		return listaDTO;
		
	}
	
	public List<ProductDTO> getProducts(CategoryDTO category){
	
		Query query = manager.createQuery("select t from Product as t where productCategory = " + category.getId());
		
		List<Product> lista = query.getResultList();
		List<ProductDTO> listaDTO = new ArrayList<ProductDTO>();
		
		for (Product t : lista){
			System.out.println(t.getProductId() + " - " + t.getProductName());
			listaDTO.add(mappingDTO(t));
		}
		
		manager.close();
		return listaDTO;
		
	}
	
	public ProductDTO mappingDTO(Product product){
		
		ProductDTO productDto = new ProductDTO();
		productDto.setId(product.getProductId());
		productDto.setName(product.getProductName());
		productDto.setShortName(product.getProductShortname());
		productDto.setDescription(product.getProductDescription());
		productDto.setImagePath(product.getProductImagePath());
		productDto.setPrice(product.getProductPrice());
		productDto.setCategory(new CategoryManager().mappingDTO(product.getProductCategory()));
		return productDto;
		
	}
	public Product mapping(ProductDTO productDto){
		
		Product product = new Product();
		product.setProductId(productDto.getId());
		product.setProductName(productDto.getName());
		product.setProductShortname(productDto.getShortName());
		product.setProductDescription(productDto.getDescription());
		product.setProductImagePath(productDto.getImagePath());
		product.setProductPrice(productDto.getPrice());
		product.setProductCategory(new CategoryManager().mapping(productDto.getCategory()));
		return product;
		
	}
}
