package com.leitmotiff.business.product.dto;

import com.leitmotiff.business.category.dto.CategoryDTO;

public class ProductDTO {
	
	private Long id;
	private String name;
	private String shortName;
	private Double price;
	private String imagePath;
	private String description;
	private CategoryDTO category;
	/*private CompanyDTO company;*/
	
	public Long getId() {
		return id;
	}
	public CategoryDTO getCategory() {
		return category;
	}
	public void setCategory(CategoryDTO category) {
		this.category = category;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}	
}
